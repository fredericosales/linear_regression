#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Frederico Sales
<frederico.sales@engenharia.ufjf.br>
2018
"""

# imports
import numpy as np
import pandas as pd
import matplotlib; matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import seaborn as sns
import math
import pdb
import logging
import warnings
import time
import datetime

def warn(*args, **kwargs):
    pass
warnings.warn = warn

# ========================================================================= #
# debug

def debug(debug=False):
    """Set pdb trace."""
    if debug == True:
        pdb.set_trace()


# ========================================================================= #
# logging

TS = time.time()
DT = datetime.date.today()
DATE = datetime.datetime.fromtimestamp(TS).strftime('%d-%m-%Y %H:%M:%S')
FORMAT = ''
LEVEL = logging.INFO
LOG_FILENAME = 'log/linear_regression-' + str(DT) + '.log'
logging.basicConfig(filename=LOG_FILENAME, level=LEVEL, format=FORMAT)


# ========================================================================= #
# dataframe
sns.set()

matrix = np.array([[1, 2, 3, 4, 5 , 6, 7, 8, 9, 10],
                   [300, 350, 500, 700, 800, 850, 900, 950, 1000, 1200]])

matriz = np.array([[1, 300],
                   [2, 350],
                   [3, 500],
                   [4, 700],
                   [5, 800],
                   [6, 850],
                   [7, 900],
                   [8, 950],
                   [9, 1000],
                   [10, 1200]])

df = pd.DataFrame(matriz, columns=['x', 'f(x)'])
X = df['x']
y = df['f(x)']


# ========================================================================= #
# coeficientes

def estimar_coef(x, y):
  """Auto explicativo"""
  
  # tamanho do dataset ou numero de observaçõeos/pontos.
  n = np.size(x)

  # media de x e y
  x_med, y_med = np.mean(x), np.mean(y)

  # calcular a derivação cruzada e derivação em função de x
  SS_xy = np.sum(y*x - n * y_med * x_med)
  SS_xx = np.sum(x*x - n * x_med * x_med)

  # regressão dos coeficientes
  b_1 = SS_xy / SS_xx
  b_0 = y_med - b_1 * x_med

  return(b_0, b_1)


# ========================================================================= #
# plotar a regressão linear

def plot_regressao_linear(x, y, b):
  """Duh can you imagine?"""

  plt.scatter(x, y, color="m", marker="o", s=30)
  plt.title('Regressao Linear')

  # vetor com as predições
  y_pred = b[0] + b[1] * x

  # linha da regressão
  plt.plot(x, y_pred, color="g")

  # eixos
  plt.xlabel('x')
  plt.ylabel('f(x)')

  # salva o plot
  plt.savefig('img/lin_reg_' + str(DT) + '.png')

  # magic
  plt.show()


# ========================================================================= #

def main():
  """Some lazy stuff."""

  x = np.array([1, 2, 3, 4, 5 , 6, 7, 8, 9, 10])
  y = np.array([300, 350, 500, 700, 800, 850, 900, 900, 1000, 1200])

  # estimando os coeficiente
  b = estimar_coef(x, y)

  logging.info(DATE)
  logging.info("# ========================== #")
  logging.info("Coeficientes estimados:\nb_0 = {:10.6f} \nb_1 = {:10.6f}".format(b[0], b[1]))
  logging.info("# ========================== #\n\n")

  # plotando
  plot_regressao_linear(x, y, b)

# ========================================================================= #

if __name__ == '__main__':
    """Do some."""
    main()
